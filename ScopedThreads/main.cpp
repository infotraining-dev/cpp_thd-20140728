#include <iostream>
#include <thread>
#include <stdexcept>

using namespace std;

void task1(int id, int delay)
{
    for(int i = 0; i < 10; ++i)
    {
        cout << "THD#" << id
             << "; i = " << i << endl;

        this_thread::sleep_for(chrono::milliseconds(delay));
    }

    cout << "THD#" << id << " has finished..." << endl;
}

void task2(int id, int& arg, int delay)
{
    for(int i = 0; i < 10; ++i)
    {
        cout << "Task#" << id << ++arg << endl;
        this_thread::sleep_for(chrono::milliseconds(delay));
    }
}

void may_throw(int i)
{
    if (i % 13 == 0)
        throw runtime_error("Error#13");
}

class ThreadJoinGuard
{
    thread& thd_;
public:
    ThreadJoinGuard(thread& thd) : thd_(thd)
    {}

    ThreadJoinGuard(const ThreadJoinGuard&) = delete;
    ThreadJoinGuard& operator=(const ThreadJoinGuard&) = delete;

    ~ThreadJoinGuard()
    {
        if (thd_.joinable())
            thd_.join();
    }
};

class ScopedThread
{
    thread thd_;

public:
    ScopedThread(thread&& thd) : thd_(move(thd))
    {}

    template <typename... T>
    ScopedThread(T&&... args) : thd_(forward<T>(args)...)
    {}

    ScopedThread(const ScopedThread&) = delete;
    ScopedThread& operator=(const ScopedThread&) = delete;

    thread& get()
    {
        return thd_;
    }

    ~ScopedThread()
    {
        if (thd_.joinable())
            thd_.join();
    }
};

void local_function()
{
    int x = 0;

    thread thd1(&task1, 1, 200);
    thread thd2(&task2, 1, ref(x), 300);

    // 1-szy sposób - RAII
    ThreadJoinGuard thread1_guard(thd1);
    ThreadJoinGuard thread2_guard(thd2);

    // 2-gi sposob

    ScopedThread scoped_thread1(thread(&task1, 2, 100));
    ScopedThread scoped_thread2(&task1, 3, 300);

    for(int i = 0; i < 20; ++i)
        may_throw(i);
}

int main()
{
    try
    {
        local_function();
    }
    catch(const runtime_error& e)
    {
        cout << "Caught exception : " << e.what() << endl;
    }
}
