#include <iostream>
#include <boost/algorithm/string.hpp>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>


using namespace std;

string text;
int x;
bool is_ready {false};

mutex mtx;
condition_variable cv;

void prepare()
{
    cout << "Start prepare..." << endl;
    text = "Hello threads";
    x = 102;

    this_thread::sleep_for(chrono::seconds(3));

    lock_guard<mutex> lk(mtx);
    is_ready = true;

    cout << "Notification..." << endl;
    cv.notify_all();
}


void consume(int id)
{
    cout << "Start consumer#" << id << endl;
    std::unique_lock<std::mutex> lk(mtx);

    cv.wait(lk, [] { return is_ready; });

    lk.unlock();

    string upper_case_text = boost::to_upper_copy(text);
    int square = x * x;

    cout << "THD#" << id << " - text: " << upper_case_text << endl;
    cout << "THD#" << id << " - square: " << square << endl;
}

int main()
{
    thread thd1(&prepare);
    thread thd2(&consume, 1);
    thread thd3(&consume, 2);
    thread thd4(&consume, 3);

    thd1.join();
    thd2.join();
    thd3.join();
    thd4.join();
}
