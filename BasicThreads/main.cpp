#include <iostream>
#include <thread>
#include <chrono>
#include <cassert>
#include <vector>
#include <functional>
#include <boost/ref.hpp>

using namespace std;

void hello_threads(int id, int delay)
{
    for(int i = 0; i < 10; ++i)
    {
        cout << "Hello threads... THD#" << id
             << "; i = " << i << endl;

        this_thread::sleep_for(chrono::milliseconds(delay));
    }

    cout << "THD#" << id << " has finished..." << endl;
}

class BackgroundWork
{
    const int id_;
    int data_ = 0;
public:
    BackgroundWork(int id) : id_{id}
    {}

    void operator()(int delay)
    {
        for(int i = 1; i <= 10; ++i)
        {
            cout << "BW#" << id_ << ": " << i << endl;

            this_thread::sleep_for(chrono::milliseconds(delay));
        }

        cout << "BW#" << id_ << " has finished..." << endl;

        data_ = 13;
    }

    int data() const
    {
        return data_;
    }
};

class Buffer
{
    std::vector<int> buffer_;
public:
    void assign(const vector<int>& source)
    {
        buffer_.assign(source.begin(), source.end());
    }

    std::vector<int> data() const
    {
        return buffer_;
    }
};

void deamon()
{
    while (true)
    {
        cout << "Deamon works..." << endl;
        this_thread::sleep_for(chrono::seconds(1));
    }
}

void run_as_deamon()
{
    thread deamon_thd(&deamon);

    deamon_thd.detach();
}

int main()
{
    run_as_deamon();
    thread thd1(&hello_threads, 1, 100);
    thread thd2(&hello_threads, 2, 300);
    thread thd3(&hello_threads, 3, 200);

    BackgroundWork bw1(1);

    thread thd4(ref(bw1), 200);
    thread thd5(BackgroundWork{2}, 100);

    vector<int> data = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    Buffer buff1;
    Buffer buff2;

    thread thd6(bind(&Buffer::assign, ref(buff1), cref(data)));

    thread thd7([&] { buff2.assign(data); } );

    thd1.join();
    thd2.detach();

    assert(thd2.joinable() == false);

    thd3.join();
    thd4.join();

    cout << "Data from bw1: " << bw1.data() << endl;
    thd5.join();

    thd6.join();
    thd7.join();

    cout << "Buff1: ";
    for(auto item : buff1.data())
    {
        cout << item << " ";
    }
    cout << endl;

    cout << "Buff2: ";
    for(auto item : buff2.data())
    {
        cout << item << " ";
    }
    cout << endl;
}
