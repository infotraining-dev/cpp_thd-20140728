#include <iostream>
#include <future>
#include <chrono>
#include <thread>

using namespace std;

typedef string FileContent;

FileContent download_file(const string& url)
{
    cout << "Start reading a file: " << url << endl;

    this_thread::sleep_for(chrono::milliseconds(2000));

    return "Content of file: " + url;
}

shared_future<FileContent> download_file_async(const string& url)
{
    return shared_future<FileContent>(async(launch::async, &download_file, url));
}

int main()
{
    cout << "Start main..." << endl;

    shared_future<FileContent> sf1 = download_file_async("test.txt");

    shared_future<FileContent> sf2 = sf1;

    cout << sf1.get() << endl;
    cout << sf2.get() << endl;
}
