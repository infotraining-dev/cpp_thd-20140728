#include <iostream>
#include <thread>
#include <mutex>
#include <boost/thread.hpp>


using namespace std;

class BankAccount
{
    typedef recursive_mutex Mutex;

    const int id_;
    double balance_;
    mutable Mutex mtx_;

public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print() const
    {
        std::cout << "Bank Account #" << id_ << "; Balance = ";
        cout << balance() << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {  
        unique_lock<Mutex> lk_from(mtx_, defer_lock);
        unique_lock<Mutex> lk_to(to.mtx_, defer_lock);

        boost::lock(lk_from, lk_to);

        withdraw(amount);
        to.deposit(amount);
    }

    void withdraw(double amount)
    {
        lock_guard<Mutex> lk(mtx_);
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        lock_guard<Mutex> lk(mtx_);
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        lock_guard<Mutex> lk(mtx_);
        return balance_;
    }

    void lock()
    {
        mtx_.lock();
    }

    void unlock()
    {
        mtx_.unlock();
    }
};

void make_deposits(BankAccount& account, int no_of_operations)
{
    for(int i = 0; i < no_of_operations; ++i)
        account.deposit(1.0);
}

void make_withdraws(BankAccount& account, int no_of_operations)
{
    for(int i = 0; i < no_of_operations; ++i)
        account.withdraw(1.0);
}

void make_transfers(BankAccount& from, BankAccount& to, int no__of_operations)
{
    for(int i = 0; i < no__of_operations; ++i)
    {
        cout << "Transfer from #" << from.id() << " to #" << to.id()
             << " in thread#" << this_thread::get_id() << endl;
        from.transfer(to, 1.0);
    }
}

int main()
{
    const int no_of_operations = 100000;

    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    ba1.print();
    ba2.print();

    cout << "\ndeposits and withdrawals\n";

    thread thd1(&make_withdraws, ref(ba1), no_of_operations);
    thread thd2(&make_deposits, ref(ba1), no_of_operations);

    thread thd3(&make_transfers, ref(ba1), ref(ba2), no_of_operations);
    thread thd4(&make_transfers, ref(ba2), ref(ba1), no_of_operations);

    thd1.join();
    thd2.join();
    thd3.join();
    thd4.join();

    ba1.print();
    ba2.print();

    {
        lock_guard<BankAccount> lk(ba1);
        ba1.withdraw(100);
        ba1.withdraw(200);
    }
}

