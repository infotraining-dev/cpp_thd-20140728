#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <atomic>

using namespace std;

const int max_iterations = 10000000;
int counter = 0;
mutex mtx;

void worker()
{
    for (int i = 0 ; i < max_iterations ; ++i)
    {
        lock_guard<mutex> lk(mtx);
        counter++;
    }
}

atomic<int> atomic_counter(0);

void worker_with_atomics()
{
    for (int i = 0 ; i < max_iterations ; ++i)
    {
        //atomic_counter++;
        atomic_counter.fetch_add(1, memory_order_relaxed);
    }
}

int main()
{
    {
        auto start = chrono::high_resolution_clock::now();

        thread th1(worker);
        thread th2(worker);
        th1.join();
        th2.join();

        auto end = chrono::high_resolution_clock::now();
        float mseconds = chrono::duration_cast<chrono::milliseconds>(end-start).count();
        cout << "Time: " << mseconds << " msec" << endl;

        cout << "Counter = " << counter << endl;
    }

    cout << "\n--------------------------\n";

    {
        auto start = chrono::high_resolution_clock::now();

        thread th1(worker_with_atomics);
        thread th2(worker_with_atomics);
        th1.join();
        th2.join();

        auto end = chrono::high_resolution_clock::now();
        float mseconds = chrono::duration_cast<chrono::milliseconds>(end-start).count();
        cout << "Time: " << mseconds << " msec" << endl;

        cout << "Counter = " << atomic_counter << endl;
    }
}

