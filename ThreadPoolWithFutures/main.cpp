#include <iostream>
#include <thread>
#include <future>
#include <functional>
#include <vector>
#include <cassert>
#include <stdexcept>
#include <memory>
#include "thread_safe_queue.hpp"

typedef std::function<void()> Task;

const std::nullptr_t END_OF_WORK = nullptr;

class ThreadPool
{
public:
    ThreadPool()
    {
        size_t no_of_threads = std::max(1u, std::thread::hardware_concurrency());

        for(size_t i = 0; i < no_of_threads; ++i)
            threads_.emplace_back([this] { run(); });
    }

    ThreadPool(size_t no_of_threads)
    {
        assert(no_of_threads >= 1);

        for(size_t i = 0; i < no_of_threads; ++i)
            threads_.emplace_back([this] { run(); });
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    ~ThreadPool()
    {
        for(size_t i = 0; i < threads_.size(); ++i)
            task_queue_.push(END_OF_WORK);

        for(auto& t : threads_)
            t.join();
    }

    template <typename F>
    auto submit(F f) -> std::future<decltype(f())>
    {
        typedef decltype(f()) Result;

        auto task = std::make_shared<std::packaged_task<Result()>>(f);
        std::future<Result> fresult = task->get_future();
        task_queue_.push([task] { (*task)(); });

        return fresult;
    }

private:
    ThreadSafeQueue<Task> task_queue_;
    std::vector<std::thread> threads_;

    void run()
    {
        while (true)
        {
            Task task;

            task_queue_.wait_and_pop(task);

            if (task == END_OF_WORK)
                return;

            task(); // calling a task
        }
    }
};


using namespace std;

void do_something()
{

}

using FileContent = string;

void save_file(const string& url, const FileContent& content)
{
    cout << "Start writing a file: " << url << " - " << content << endl;

    this_thread::sleep_for(chrono::milliseconds(2000));

    cout << "End writing a file: " << url << endl;
}

FileContent download_file(const string& url)
{
    cout << "Start reading a file: " << url << endl;

    this_thread::sleep_for(chrono::milliseconds(2000));

    return "Content of file: " + url;
}

int main()
{
    ThreadPool thd_pool(100);

    auto f1 = thd_pool.submit(&do_something);
    thd_pool.submit([] { save_file("test0.txt", "Text"); });

    future<FileContent> future_content =
            thd_pool.submit([] { return download_file("testA.txt"); });

    cout << future_content.get() << endl;
    f1.wait();
}
