#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>
#include <mutex>
#include <functional>
#include <boost/lexical_cast.hpp>

#include "active_object.hpp"

using namespace std;

namespace Before
{

class Logger
{
    ofstream fout_;
    mutex mtx_;
public:
    Logger(const string& file_name)
    {
        fout_.open(file_name);
    }

    ~Logger()
    {
        fout_.close();
    }

    void log(const string& message)
    {
        lock_guard<mutex> lk(mtx_);

        fout_ << message << endl;
        fout_.flush();
    }
};

}

namespace After
{
    class Logger
    {
        ofstream fout_;
        ActiveObject ao_;
    public:
        Logger(const string& file_name)
        {
            fout_.open(file_name);
        }

        ~Logger()
        {
            ao_.send([=] { fout_.close(); });
        }

        void log(const string& message)
        {
            ao_.send([message, this] { do_log(message);});
        }

    private:
        void do_log(const string& message)
        {
            fout_ << message << endl;
            //fout_.flush();
        }
    };
}

using namespace After;

void run(Logger& logger, int id)
{
    for(int i = 0; i < 1000; ++i)
        logger.log("Log#" + boost::lexical_cast<string>(id) + " - Event#" + boost::lexical_cast<string>(i));
}

int main()
{
    /*
     * Napisz klase Logger, ktora jest thread-safe
     */

    Logger log("data.log");

    thread thd1(&run, ref(log), 1);
    thread thd2(&run, ref(log), 2);

    thd1.join();
    thd2.join();
}
