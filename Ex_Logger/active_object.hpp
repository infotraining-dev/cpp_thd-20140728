#ifndef ACTIVE_OBJECT_HPP
#define ACTIVE_OBJECT_HPP

#include <thread>
#include <functional>
#include "thread_safe_queue.hpp"

typedef std::function<void()> Task;

class ActiveObject
{
public:
    ActiveObject() : thread_([this] { run();})
    {
    }

    ActiveObject(const ActiveObject&) = delete;
    ActiveObject& operator=(const ActiveObject) = delete;

    ~ActiveObject()
    {
        send([this] { is_done_ = true; });

        thread_.join();
    }

    void send(Task task)
    {
        task_queue_.push(task);
    }

private:
    ThreadSafeQueue<Task> task_queue_;
    std::thread thread_;
    bool is_done_ = false;

    void run()
    {
        while (true)
        {
            Task task;

            task_queue_.wait_and_pop(task);

            task();

            if (is_done_)
                return;
        }
    }
};


#endif // ACTIVE_OBJECT_HPP
