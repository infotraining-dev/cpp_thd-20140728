#ifndef THREAD_SAFE_QUEUE_HPP
#define THREAD_SAFE_QUEUE_HPP

#include <queue>
#include <mutex>
#include <condition_variable>

template <typename T>
class ThreadSafeQueue
{
    std::queue<T> queue_;
    std::mutex mtx_;
    std::condition_variable cv_;
public:
    void push(const T& item)
    {
        std::lock_guard<std::mutex> lk(mtx_);
        queue_.push(item);
        cv_.notify_one();
    }

    void wait_and_pop(T& item)
    {
        std::unique_lock<std::mutex> lk(mtx_);
        cv_.wait(lk, [this] { return !queue_.empty(); });
        item = queue_.front();
        queue_.pop();
    }

    bool try_pop(T& item)
    {
        std::lock_guard<std::mutex> lk(mtx_);

        if (queue_.empty())
            return false;

        item = queue_.front();
        queue_.pop();

        return true;
    }
};

#endif // THREAD_SAFE_QUEUE_HPP
