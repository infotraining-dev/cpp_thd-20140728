#include <iostream>
#include <algorithm>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include "thread_safe_queue.hpp"

using namespace std;

typedef string Data;

const Data end_of_work = "";

class ProducerConsumer
{
    ThreadSafeQueue<Data> data_queue_;
public:
    void produce(const Data& data, size_t consumer_count)
    {
        Data item = data;

        while(next_permutation(item.begin(), item.end()))
        {
            cout << "Producing: " << item << endl;
            data_queue_.push(item);
            this_thread::sleep_for(chrono::milliseconds(100));
        }

        cout << "Producer has finished its job..." << endl;

        for(size_t i = 0; i < consumer_count; ++i)
        {
            data_queue_.push(end_of_work);
        }
    }

    void consume(int id)
    {
        while (true)
        {
            Data item;

            data_queue_.wait_and_pop(item);

            if (item == end_of_work)
                break;

            process_data(id, item);
        }

        cout << "Consumer#" << id << " has finished its job..." << endl;
    }
public:
    void process_data(int id, const Data& item)
    {
        cout << "Consumer#" << id << " is processing " << item << endl;
        this_thread::sleep_for(chrono::milliseconds(100));
    }
};

int main()
{
    ProducerConsumer pc;

    thread thd1([&pc] { pc.produce("abcd", 2); });
    thread thd2(&ProducerConsumer::consume, ref(pc), 1);
    thread thd3(&ProducerConsumer::consume, ref(pc), 2);

    thd1.join();
    thd2.join();
    thd3.join();
}
