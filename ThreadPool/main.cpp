#include <iostream>
#include <thread>
#include <future>
#include <functional>
#include <vector>
#include <cassert>
#include <stdexcept>
#include "thread_safe_queue.hpp"

typedef std::function<void()> Task;

const std::nullptr_t END_OF_WORK = nullptr;

class ThreadPool
{
public:
    ThreadPool()
    {
        size_t no_of_threads = std::max(1u, std::thread::hardware_concurrency());

        for(size_t i = 0; i < no_of_threads; ++i)
            threads_.emplace_back([this] { run(); });
    }

    ThreadPool(size_t no_of_threads)
    {
        assert(no_of_threads >= 1);

        for(size_t i = 0; i < no_of_threads; ++i)
            threads_.emplace_back([this] { run(); });
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    ~ThreadPool()
    {
        for(size_t i = 0; i < threads_.size(); ++i)
            task_queue_.push(END_OF_WORK);

        for(auto& t : threads_)
            t.join();
    }

    void submit(Task task)
    {
        if (task == nullptr)
            throw std::invalid_argument("Task can't be null");

        task_queue_.push(task);
    }

private:
    ThreadSafeQueue<Task> task_queue_;
    std::vector<std::thread> threads_;

    void run()
    {
        while (true)
        {
            Task task;

            task_queue_.wait_and_pop(task);

            if (task == END_OF_WORK)
                return;

            task(); // calling a task
        }
    }
};


using namespace std;

void do_something()
{

}

using FileContent = string;

void save_file(const string& url, const FileContent& content)
{
    cout << "Start writing a file: " << url << " - " << content << endl;

    this_thread::sleep_for(chrono::milliseconds(2000));

    cout << "End writing a file: " << url << endl;
}

int main()
{
    ThreadPool thd_pool(100);

    thd_pool.submit(&do_something);
    thd_pool.submit([] { save_file("test0.txt", "Text"); });

    for(int i = 0; i < 100; ++i)
    {
        string filename = "test" + to_string(i + 1) + ".txt";
        thd_pool.submit([=] { save_file(filename, "text"); });
    }
}
