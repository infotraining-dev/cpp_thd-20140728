#include <iostream>
#include <thread>
#include <stdexcept>
#include <exception>
#include <vector>

using namespace std;

void may_throw(int id, int arg)
{
    if (arg == 13)
        throw runtime_error("Error#13 from THD#" + to_string(id));
}

void background_work(int id, size_t count, exception_ptr& e)
{
    try
    {
        for(int i = 0; i < count; ++i)
        {
            cout << "THD#" << id << ": " << i << endl;

            this_thread::sleep_for(chrono::milliseconds(100));

            may_throw(id, i);
        }

        cout << "THD#" << id << " has finished..." << endl;
    }
    catch(...)
    {
        cout << "Catch in THD#" << id << endl;

        e = current_exception();

        cout << "Exception stored..." << endl;
    }
}

int main() try
{
    cout << "Hardware concurrency: "
         << thread::hardware_concurrency() << endl;

    const int no_of_threads = 4;

    vector<thread> threads(no_of_threads);
    vector<exception_ptr> exceptions(no_of_threads);

    for(int i = 0; i < no_of_threads; ++i)
        threads[i] = thread(&background_work, i+1, (i+1) * 10,
                            ref(exceptions[i]));

    for(auto& thd : threads)
        thd.join();

    // obsluga bledow
    for(auto& e : exceptions)
    {
        if (e)
        {
            try
            {
                rethrow_exception(e);
            }
            catch(const runtime_error& e)
            {
                cout << "Handling an exception: " << e.what() << endl;;
            }
        }
    }
}
catch(const exception& e)
{
    cout << "Main catch: " << e.what() << endl;
}
