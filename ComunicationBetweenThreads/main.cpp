#include <iostream>
#include <boost/algorithm/string.hpp>
#include <thread>
#include <mutex>
#include <atomic>


using namespace std;

string text;
int x;
atomic<bool> is_ready {false};

void prepare()
{
    text = "Hello threads";
    x = 102;

    is_ready.store(true, memory_order_release);
}


void consume(int id)
{
    while (!is_ready.load(memory_order_acquire))
    {
    }

    string upper_case_text = boost::to_upper_copy(text);
    int square = x * x;

    cout << "THD#" << id << " - text: " << upper_case_text << endl;
    cout << "THD#" << id << " - square: " << square << endl;
}

int main()
{
    thread thd1(&prepare);
    thread thd2(&consume, 1);

    thd1.join();
    thd2.join();
}
