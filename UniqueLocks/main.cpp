#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

typedef timed_mutex Mutex;

Mutex mtx;

void background_worker(int id, int timeout)
{
    cout << "THD#" << id << " is waiting for mutex..." << endl;

    unique_lock<Mutex> lk(mtx, try_to_lock);

    if (!lk.owns_lock())
    {
        do
        {
            cout  << "THD#" << id << " doesn't own lock..."
                  << " Tries to aquire mutex..." << endl;

        } while (!lk.try_lock_for(chrono::milliseconds(1000)));
    }

    cout << "Start THD#" << id << endl;

    this_thread::sleep_for(chrono::seconds(timeout));

    lk.unlock();

    cout << "End of THD#" << id << endl;
}

int main()
{
    thread thd1(&background_worker, 1, 10);
    thread thd2(&background_worker, 2, 5);

    thd1.join();
    thd2.join();
}
