#include <iostream>
#include <thread>
#include <future>
#include <chrono>
#include <random>
#include <stdexcept>

using namespace std;

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_int_distribution<> dis(1000, 2000);

int calculate_square(int x)
{
    cout << "Starting calculation for " << x << endl;

    int delay = dis(gen);

    this_thread::sleep_for(chrono::milliseconds(delay));

    cout << "End of calculation for " << x << endl;

    if (x % 13 == 0)
        throw runtime_error("Error#13");

    return x * x;
}

void may_throw(int arg)
{
    if (arg % 13 == 0)
        throw runtime_error("Error#13");
}

class SquareCalulator
{
    promise<int> promise_;
public:
    void operator()(int x)
    {
        cout << "SquareCalc starts job..." << endl;

        int delay = dis(gen);

        this_thread::sleep_for(chrono::milliseconds(delay));

        try
        {
            may_throw(x);
        }
        catch(...)
        {
            promise_.set_exception(current_exception());
            return;
        }

        promise_.set_value(x * x);

        cout << "SquareCalc ends job..." << endl;
    }

    std::future<int> make_future()
    {
        return promise_.get_future();
    }
};

int main()
{
    // synchronicznie

    int r1 = calculate_square(2);
    int r2 = calculate_square(5);
    int r3 = calculate_square(77);

    cout << "r1 = " << r1 << endl;
    cout << "r2 = " << r2 << endl;
    cout << "r3 = " << r3 << endl;

    cout << "\n\n";


    // asynchronicznie
    // 1-szy sposob - packaged_task
    packaged_task<int()> pt1([] { return calculate_square(2);});
    packaged_task<int()> pt2(bind(&calculate_square, 9));
    packaged_task<int(int)> pt3(&calculate_square);
    packaged_task<int(int)> pt4(&calculate_square);

    future<int> f1 = pt1.get_future();
    future<int> f2 = pt2.get_future();
    auto f3 = pt3.get_future();
    auto f4 = pt4.get_future();

    thread thd1(move(pt1));
    thread thd2(move(pt2));
    thread thd3(move(pt4), 26);
    pt3(77);

    thd1.detach();
    thd2.detach();
    thd3.detach();

    try
    {
        cout << "f1 = " << f1.get() << endl;
        cout << "f2 = " << f2.get() << endl;
        cout << "f3 = " << f3.get() << endl;
        cout << "f4 = " << f4.get() << endl;
    }
    catch(const runtime_error& e)
    {
        cout << "Catched: " << e.what() << endl;
    }

//    thd1.join();
//    thd2.join();

    cout << "\n\n";

    // 2-gi sposob - promise
    SquareCalulator square_calc;

    auto f5 = square_calc.make_future();

    thread thd4(ref(square_calc), 26);

    try
    {
        cout << "f5 = " << f5.get() << endl;
    }
    catch(const runtime_error& e)
    {
        cout << "Catched: " << e.what() << endl;
    }

    thd4.join();

    cout << "\n\n";

    // 3. sposób - async

    auto f6 = async(launch::async,  &calculate_square, 89);
    auto f7 = async(launch::async, [] { return calculate_square(788);});

    cout << "f6 = " << f6.get() << endl;
    cout << "f7 = " << f7.get() << endl;

    cout << "\n\n";

    vector<future<int>> future_squares;

    for(int i = 0; i < 100; ++i)
        future_squares.push_back(async(launch::async, &calculate_square, i));

    for(auto& f : future_squares)
    {
        try
        {
            cout << "Result: " << f.get() << endl;
        }
        catch(const runtime_error& e)
        {
            cout << "Catched: " << e.what() << endl;
        }
    }
}

