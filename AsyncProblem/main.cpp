#include <iostream>
#include <future>
#include <chrono>
#include <thread>

using namespace std;

typedef string FileContent;

void save_file(const string& url, const FileContent& content)
{
    cout << "Start writing a file: " << url << " - " << content << endl;

    this_thread::sleep_for(chrono::milliseconds(2000));

    cout << "End writing a file: " << url << endl;
}

template <typename F>
auto spawn_task(F f) -> future<decltype(f())>
{
    typedef typename result_of<F()>::type Result;
    packaged_task<Result()> pt(f);
    future<Result> future_result = pt.get_future();
    thread thd(move(pt));
    thd.detach();

    return future_result;
}

int main()
{
    {
        spawn_task([] { save_file("test3.txt", "Text");});
        spawn_task([] { save_file("test4.txt", "Text");});
    }

     cout << "\n\n";

    {
        async(launch::async, &save_file, "test1.txt", "Text");
        async(launch::async, &save_file, "test2.txt", "Text");
    }

    cout << "\n\n";

    {
        auto f1 = async(launch::async, &save_file, "test1.txt", "Text");
        auto f2 = async(launch::async, &save_file, "test2.txt", "Text");
    }




}
