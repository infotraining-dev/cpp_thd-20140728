#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void background_work(int id, int delay)
{
    for(int i = 0; i < 10; ++i)
    {
        cout << "THD#" << id << ": " << i << endl;

        this_thread::sleep_for(chrono::milliseconds(delay));
    }

    cout << "END OF WORK THD#" << id;
}

thread create_thread(int id)
{
    thread thd(&background_work, id, 200);

    return move(thd);
}

int main()
{
    cout << "Main thread id: " << this_thread::get_id() << endl;
    thread thd1 = create_thread(1);
    thread thd2 = create_thread(2);
    thread thd3(&background_work, 3, 150);

    cout << thd1.get_id() << endl;
    cout << thd2.get_id() << endl;

    vector<thread> thread_group(2);

    thread_group[0] = move(thd1);
    thread_group[1] = move(thd2);
    thread_group.push_back(move(thd3));
    thread_group.push_back(thread(&background_work, 4, 50));
    thread_group.emplace_back(&background_work, 5, 80);

    cout << "\nPo move:\n";

    cout << thd1.get_id() << endl;
    cout << thd2.get_id() << endl;

    for(auto& thd : thread_group)
        thd.join();
}
